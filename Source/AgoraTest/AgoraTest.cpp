// Copyright Epic Games, Inc. All Rights Reserved.

#include "AgoraTest.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, AgoraTest, "AgoraTest" );

DEFINE_LOG_CATEGORY(LogAgoraTest)
 