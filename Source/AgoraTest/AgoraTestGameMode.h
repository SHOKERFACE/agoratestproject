// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "AgoraTestGameMode.generated.h"

UCLASS(minimalapi)
class AAgoraTestGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AAgoraTestGameMode();
};



