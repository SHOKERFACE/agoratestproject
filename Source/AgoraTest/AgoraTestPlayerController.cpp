// Copyright Epic Games, Inc. All Rights Reserved.

#include "AgoraTestPlayerController.h"
#include "GameFramework/Pawn.h"
#include "Blueprint/AIBlueprintHelperLibrary.h"
#include "NiagaraSystem.h"
#include "NiagaraFunctionLibrary.h"
#include "AgoraTestCharacter.h"
#include "Engine/World.h"
#include "EnhancedInputComponent.h"
#include "InputActionValue.h"
#include "EnhancedInputSubsystems.h"
#include "Camera/CameraComponent.h"
#include "Engine/LocalPlayer.h"

DEFINE_LOG_CATEGORY(LogTemplateCharacter);

AAgoraTestPlayerController::AAgoraTestPlayerController()
{
	bShowMouseCursor = true;
	DefaultMouseCursor = EMouseCursor::Default;
	CachedDestination = FVector::ZeroVector;
	FollowTime = 0.f;
}

void AAgoraTestPlayerController::BeginPlay()
{
	// Call the base class  
	Super::BeginPlay();

	//Add Input Mapping Context
	if (UEnhancedInputLocalPlayerSubsystem* Subsystem = ULocalPlayer::GetSubsystem<UEnhancedInputLocalPlayerSubsystem>(GetLocalPlayer()))
	{
		Subsystem->AddMappingContext(DefaultMappingContext, 0);
	}
}

void AAgoraTestPlayerController::SetupInputComponent()
{
	// set up gameplay key bindings
	Super::SetupInputComponent();

	// Set up action bindings
	if (UEnhancedInputComponent* EnhancedInputComponent = Cast<UEnhancedInputComponent>(InputComponent))
	{
		// Setup mouse input events
		EnhancedInputComponent->BindAction(SetDestinationClickAction, ETriggerEvent::Started, this, &AAgoraTestPlayerController::OnInputStarted);
		EnhancedInputComponent->BindAction(SetDestinationClickAction, ETriggerEvent::Triggered, this, &AAgoraTestPlayerController::OnSetDestinationTriggered);
		EnhancedInputComponent->BindAction(SetDestinationClickAction, ETriggerEvent::Completed, this, &AAgoraTestPlayerController::OnSetDestinationReleased);
		EnhancedInputComponent->BindAction(SetDestinationClickAction, ETriggerEvent::Canceled, this, &AAgoraTestPlayerController::OnSetDestinationReleased);

		// Setup touch input events
		EnhancedInputComponent->BindAction(SetDestinationTouchAction, ETriggerEvent::Started, this, &AAgoraTestPlayerController::OnInputStarted);
		EnhancedInputComponent->BindAction(SetDestinationTouchAction, ETriggerEvent::Triggered, this, &AAgoraTestPlayerController::OnTouchTriggered);
		EnhancedInputComponent->BindAction(SetDestinationTouchAction, ETriggerEvent::Completed, this, &AAgoraTestPlayerController::OnTouchReleased);
		EnhancedInputComponent->BindAction(SetDestinationTouchAction, ETriggerEvent::Canceled, this, &AAgoraTestPlayerController::OnTouchReleased);
		EnhancedInputComponent->BindAction(DetachCameraClickAction,ETriggerEvent::Started, this, &AAgoraTestPlayerController::SetCameraAttach);
		EnhancedInputComponent->BindAction(CameraMoveRightAction, ETriggerEvent::Triggered, this,&AAgoraTestPlayerController::SetCameraY);
		EnhancedInputComponent->BindAction(CameraMoveForwardAction, ETriggerEvent::Triggered, this,&AAgoraTestPlayerController::SetCameraX);

		EnhancedInputComponent->BindAction(InteractAction, ETriggerEvent::Started, this, &AAgoraTestPlayerController::Interact);
	}
	else
	{
		UE_LOG(LogTemplateCharacter, Error, TEXT("'%s' Failed to find an Enhanced Input Component! This template is built to use the Enhanced Input system. If you intend to use the legacy system, then you will need to update this C++ file."), *GetNameSafe(this));
	}
}

void AAgoraTestPlayerController::OnInputStarted()
{
	StopMovement();
}

// Triggered every frame when the input is held down
void AAgoraTestPlayerController::OnSetDestinationTriggered()
{
	// We flag that the input is being pressed
	FollowTime += GetWorld()->GetDeltaSeconds();
	
	// We look for the location in the world where the player has pressed the input
	FHitResult Hit;
	bool bHitSuccessful = false;
	if (bIsTouch)
	{
		bHitSuccessful = GetHitResultUnderFinger(ETouchIndex::Touch1, ECollisionChannel::ECC_Visibility, true, Hit);
	}
	else
	{
		bHitSuccessful = GetHitResultUnderCursor(ECollisionChannel::ECC_Visibility, true, Hit);
	}

	// If we hit a surface, cache the location
	if (bHitSuccessful)
	{
		CachedDestination = Hit.Location;
	}
	
	// Move towards mouse pointer or touch
	APawn* ControlledPawn = GetPawn();
	if (ControlledPawn != nullptr)
	{
		FVector WorldDirection = (CachedDestination - ControlledPawn->GetActorLocation()).GetSafeNormal();
		ControlledPawn->AddMovementInput(WorldDirection, 1.0, false);
	}
}

void AAgoraTestPlayerController::OnSetDestinationReleased()
{
	// If it was a short press
	if (FollowTime <= ShortPressThreshold)
	{
		// We move there and spawn some particles
		UAIBlueprintHelperLibrary::SimpleMoveToLocation(this, CachedDestination);
		UNiagaraFunctionLibrary::SpawnSystemAtLocation(this, FXCursor, CachedDestination, FRotator::ZeroRotator, FVector(1.f, 1.f, 1.f), true, true, ENCPoolMethod::None, true);
	}

	FollowTime = 0.f;
}

// Triggered every frame when the input is held down
void AAgoraTestPlayerController::OnTouchTriggered()
{
	bIsTouch = true;
	OnSetDestinationTriggered();
}

void AAgoraTestPlayerController::OnTouchReleased()
{
	bIsTouch = false;
	OnSetDestinationReleased();
}


void AAgoraTestPlayerController::SetCameraAttach()
{
	if (AAgoraTestCharacter* MyCharacter = Cast<AAgoraTestCharacter>(GetPawn()))
	{
		if (GetCharacterCameraComponent()->GetAttachParent())
		{
			MyCharacter->SetCharacterCameraAttach(false);
		}
		else
		{
			MyCharacter->SetCharacterCameraAttach(true);
		}

	}
}

void AAgoraTestPlayerController::Interact()
{
	if (AAgoraTestCharacter* MyCharacter = Cast<AAgoraTestCharacter>(GetPawn()))
	{
		MyCharacter->Interact();
	}
}

UCameraComponent* AAgoraTestPlayerController::GetCharacterCameraComponent()
{
	if (!TopDownCameraComponent)
	{
		if (AAgoraTestCharacter* MyCharacter = Cast<AAgoraTestCharacter>(GetPawn()))
		{
			return TopDownCameraComponent = MyCharacter->GetTopDownCameraComponent();
		}
	}

	return TopDownCameraComponent;
	
}

void AAgoraTestPlayerController::SetCameraX(const FInputActionValue& Value)
{
	if (!GetCharacterCameraComponent()) { return; }

	if (GetCharacterCameraComponent()->GetAttachParent()) { return; }

	float AxisValue = Value.Get<float>();
	
	const FVector MoveDirection(AxisValue * CameraMoveSpeed, 0.0f, 0.0f);


	GetCharacterCameraComponent()->AddWorldOffset(MoveDirection);
}

void AAgoraTestPlayerController::SetCameraY(const FInputActionValue& Value)
{
	if (!GetCharacterCameraComponent()) { return; }

	if (GetCharacterCameraComponent()->GetAttachParent()) { return; }

	float AxisValue = Value.Get<float>();
	const FVector MoveDirection(0.0f, AxisValue * CameraMoveSpeed, 0.0f);


	GetCharacterCameraComponent()->AddWorldOffset(MoveDirection);
}

