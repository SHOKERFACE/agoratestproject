// Copyright Epic Games, Inc. All Rights Reserved.

#include "AgoraTestCharacter.h"
#include "UObject/ConstructorHelpers.h"
#include "Camera/CameraComponent.h"
#include "Components/DecalComponent.h"
#include "Components/CapsuleComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/PlayerController.h"
#include "GameFramework/SpringArmComponent.h"
#include "Materials/Material.h"
#include "Engine/World.h"
#include "Interfaces/Interactable.h"


AAgoraTestCharacter::AAgoraTestCharacter()
{
	// Set size for player capsule
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);

	// Don't rotate character to camera direction
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	// Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = true; // Rotate character to moving direction
	GetCharacterMovement()->RotationRate = FRotator(0.f, 640.f, 0.f);
	GetCharacterMovement()->bConstrainToPlane = true;
	GetCharacterMovement()->bSnapToPlaneAtStart = true;

	// Create a camera boom...
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->SetUsingAbsoluteRotation(true); // Don't want arm to rotate when character does
	CameraBoom->TargetArmLength = 800.f;
	CameraBoom->SetRelativeRotation(FRotator(-60.f, 0.f, 0.f));
	CameraBoom->bDoCollisionTest = false; // Don't want to pull camera in when it collides with level

	// Create a camera...
	TopDownCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("TopDownCamera"));
	TopDownCameraComponent->SetupAttachment(CameraBoom, USpringArmComponent::SocketName);
	TopDownCameraComponent->bUsePawnControlRotation = false; // Camera does not rotate relative to arm

	// Activate ticking in order to update the cursor every frame.
	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.bStartWithTickEnabled = true;
}

void AAgoraTestCharacter::Tick(float DeltaSeconds)
{
    Super::Tick(DeltaSeconds);
	
}

void AAgoraTestCharacter::BeginPlay()
{
	Super::BeginPlay();
	GetCapsuleComponent()->OnComponentBeginOverlap.AddDynamic(this, &AAgoraTestCharacter::OnBeginOverlap);
	GetCapsuleComponent()->OnComponentEndOverlap.AddDynamic(this, &AAgoraTestCharacter::OnEndOverlap);
}

void AAgoraTestCharacter::SetCharacterCameraAttach(bool Attach)
{
	if (Attach)
	{
		TopDownCameraComponent->AttachToComponent(CameraBoom, FAttachmentTransformRules::SnapToTargetNotIncludingScale, USpringArmComponent::SocketName);
	}
	else
	{
		TopDownCameraComponent->DetachFromComponent(FDetachmentTransformRules::KeepWorldTransform);
	}
	
}

void AAgoraTestCharacter::Interact()
{
	if (!CurrentInteractActor){return;}
	
	if (CurrentInteractActor->Implements<UInteractable>())
	{
		IInteractable::Execute_Interact(CurrentInteractActor, this);
	}
	
}

void AAgoraTestCharacter::SetInteractActor(AActor* InteractActor)
{
	
	CurrentInteractActor = InteractActor;
	
}

void AAgoraTestCharacter::ClearInteractActor(AActor* InteractActor)
{
	if (CurrentInteractActor == InteractActor)
	{
		CurrentInteractActor = nullptr;
	}
}

void AAgoraTestCharacter::OnBeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor,
	UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (OtherActor->Implements<UInteractable>())
	{
		SetInteractActor(OtherActor);
	}
	
	
}

void AAgoraTestCharacter::OnEndOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor,
	UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	if (OtherActor->Implements<UInteractable>())
	{
		ClearInteractActor(OtherActor);
	}
	
}




