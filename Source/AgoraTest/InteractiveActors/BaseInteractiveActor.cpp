// Fill out your copyright notice in the Description page of Project Settings.


#include "InteractiveActors/BaseInteractiveActor.h"
#include "Components/BoxComponent.h"



// Sets default values
ABaseInteractiveActor::ABaseInteractiveActor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;
	MeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MeshComponent"));
	SetRootComponent(MeshComponent);
	SetReplicates(true);
	BoxCollision = CreateDefaultSubobject<UBoxComponent>(TEXT("BoxCollision"));
	BoxCollision->SetupAttachment(MeshComponent);
	BoxCollision->SetGenerateOverlapEvents(true);
	
}

// Called when the game starts or when spawned
void ABaseInteractiveActor::BeginPlay()
{
	Super::BeginPlay();
	
}


void ABaseInteractiveActor::Interact_Implementation(AActor* InteractedActor)
{
	//in bp
}



