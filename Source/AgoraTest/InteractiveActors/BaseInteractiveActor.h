// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Interfaces/Interactable.h"

#include "BaseInteractiveActor.generated.h"

UCLASS()
class AGORATEST_API ABaseInteractiveActor : public AActor, public IInteractable
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ABaseInteractiveActor();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	class UStaticMeshComponent* MeshComponent;
	
	UPROPERTY(EditDefaultsOnly)
	class UBoxComponent* BoxCollision;

	
	
	virtual void Interact_Implementation(AActor* InteractedActor);
	
	

};
